# ansible-roles-generic
Generic Ansible roles for re-use in playbooks

Roles here should be useful for everyone, regardless of context, and be usable independently.
