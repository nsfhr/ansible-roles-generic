hhvm support has not yet been completely fleshed out.

However, it works in principle with the php-fpm style setup,
this role and some tweaks to /etc/hhvm/server.ini, so should
we ever decide to run hhvm in production, no fundamental changes
will be required.

